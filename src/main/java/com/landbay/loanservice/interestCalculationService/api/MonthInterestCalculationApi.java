package com.landbay.loanservice.interestCalculationService.api;

import com.landbay.loanservice.interestCalculationService.domain.InvestmentsInterest;
import com.landbay.loanservice.interestCalculationService.service.MonthLoanCalculationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MonthInterestCalculationApi {

    MonthLoanCalculationService monthLoanCalculationService;

    @RequestMapping(value = "/monthly-interest/{loanId}", method = RequestMethod.GET)
    public ResponseEntity calculateInterest(@PathVariable("loanId") String loanId) {

        try {
            InvestmentsInterest investmentsInterest= monthLoanCalculationService.calculateLoan(Integer.parseInt(loanId));
            return ResponseEntity.ok(investmentsInterest);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("An error occurred while calculating interest");
        }

    }

    @Autowired
    public MonthInterestCalculationApi(MonthLoanCalculationService monthLoanCalculationService) {
        this.monthLoanCalculationService = monthLoanCalculationService;
    }
}
