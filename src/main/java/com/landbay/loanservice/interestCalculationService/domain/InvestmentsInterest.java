package com.landbay.loanservice.interestCalculationService.domain;

import java.util.List;

public class InvestmentsInterest {

    private int loanId;
    private double loanValue;
    private List<Investments> investments;

    public int getLoanId(){
        return loanId;
    }

    public void setLoanId(int loanId){
        this.loanId = loanId;
    }

    public double getLoanValue(){
        return loanValue;
    }

    public void setLoanValue(double loanValue){
        this.loanValue = loanValue;
    }

    public void setLoanInvestments(List<Investments> investments) { this.investments = investments; }

    public List<Investments> getLoanInvestments(){
        return investments;
    }

}
