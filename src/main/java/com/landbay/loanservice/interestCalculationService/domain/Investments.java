package com.landbay.loanservice.interestCalculationService.domain;

public class Investments {

    private int investmentId;
    private double investmentAmount;
    private double interestOwed;


    public void setInvestmentId(int investmentId){
        this.investmentId = investmentId;
    }

    public int getInvestmentId(){
        return investmentId;
    }

    public void setInvestmentAmount(double investmentAmount){
        this.investmentAmount = investmentAmount;
    }

    public double getInvestmentAmount(){
        return investmentAmount;
    }

    public void setInterestOwed(double interestOwed){
        this.interestOwed = interestOwed;
    }

    public double getInterestOwed(){
        return interestOwed;
    }
}
