package com.landbay.loanservice.interestCalculationService.service;

import com.landbay.loanservice.interestCalculationService.domain.InvestmentsInterest;
import com.landbay.loanservice.interestCalculationService.domain.Investments;
import com.landbay.loanservice.loanManagement.domain.LoanInvestments;
import com.landbay.loanservice.loanManagement.domain.LoanInvestmentsPayload;
import com.landbay.loanservice.loanManagement.service.LoanInvestmentDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MonthLoanCalculationService {

    LoanInvestmentDetailsService loanInvestmentDetailsService;

    public InvestmentsInterest calculateLoan(int loanId) {

        LoanInvestmentsPayload loanInvestmentsPayload = loanInvestmentDetailsService.retrieveLoan(loanId);

        double interestRate = loanInvestmentsPayload.getInterestRate();

        List<LoanInvestments> investments = loanInvestmentsPayload.getLoanInvestments();


        InvestmentsInterest investmentsInterest = new InvestmentsInterest();
        investmentsInterest.setLoanId(loanId);
        investmentsInterest.setLoanValue(loanInvestmentsPayload.getLoanValue());

        List<Investments> loanInvestmentInterests = new ArrayList<>();

        for (LoanInvestments loanInvestments : investments) {
            Investments investment = new Investments();

            investment.setInvestmentId(loanInvestments.getLenderId());
            investment.setInvestmentAmount(loanInvestments.getInitialAmount());
            investment.setInterestOwed(((interestRate/100)/12) * loanInvestments.getInitialAmount());

            loanInvestmentInterests.add(investment);
        }

        investmentsInterest.setLoanInvestments(loanInvestmentInterests);

        return investmentsInterest;

    }

    @Autowired
    public MonthLoanCalculationService(LoanInvestmentDetailsService loanInvestmentDetailsService) {
        this.loanInvestmentDetailsService = loanInvestmentDetailsService;
    }
}
