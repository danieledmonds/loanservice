package com.landbay.loanservice.interestMultipleTimePeriods.api;

import com.landbay.loanservice.common.DateUtils;
import com.landbay.loanservice.interestCalculationService.domain.InvestmentsInterest;
import com.landbay.loanservice.interestMultipleTimePeriods.service.VariableTimeLoanCalculationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.text.ParseException;

@RestController
public class VariableTimeInterestCalculationApi {

    VariableTimeLoanCalculationService variableTimeLoanCalculationService;

    @RequestMapping(value = "/variable-time-interest", method = RequestMethod.GET)
    public ResponseEntity calculateInterest(@PathParam("loanId") String loanId,
                                            @PathParam("startDate") String startDate,
                                            @PathParam("endDate") String endDate) {

        try {
            InvestmentsInterest investmentsInterest = variableTimeLoanCalculationService.calculateLoan(Integer.parseInt(loanId), DateUtils.toSqlDate(startDate), DateUtils.toSqlDate(endDate));
            return ResponseEntity.ok(investmentsInterest);
        } catch (IllegalArgumentException | ParseException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("An error occurred while calculating interest");
        }

    }

    @Autowired
    public VariableTimeInterestCalculationApi( VariableTimeLoanCalculationService variableTimeLoanCalculationService) {
        this.variableTimeLoanCalculationService = variableTimeLoanCalculationService;
    }
}
