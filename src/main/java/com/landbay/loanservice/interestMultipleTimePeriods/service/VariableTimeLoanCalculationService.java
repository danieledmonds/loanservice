package com.landbay.loanservice.interestMultipleTimePeriods.service;

import com.landbay.loanservice.interestCalculationService.domain.Investments;
import com.landbay.loanservice.interestCalculationService.domain.InvestmentsInterest;
import com.landbay.loanservice.loanManagement.domain.LoanInvestments;
import com.landbay.loanservice.loanManagement.domain.LoanInvestmentsPayload;
import com.landbay.loanservice.loanManagement.service.LoanInvestmentDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Service
public class VariableTimeLoanCalculationService {

    LoanInvestmentDetailsService loanInvestmentDetailsService;

    private static DecimalFormat df2 = new DecimalFormat(".##");

    public InvestmentsInterest calculateLoan(int loanId, java.sql.Date startDate, java.sql.Date endDate) {

        LoanInvestmentsPayload loanInvestmentsPayload = loanInvestmentDetailsService.retrieveLoan(loanId);

        double interestRate = loanInvestmentsPayload.getInterestRate();

        List<LoanInvestments> investments = loanInvestmentsPayload.getLoanInvestments();


        InvestmentsInterest investmentsInterest = new InvestmentsInterest();
        investmentsInterest.setLoanId(loanId);
        investmentsInterest.setLoanValue(loanInvestmentsPayload.getLoanValue());

        double interestRateDay = (interestRate / 100) / 365;

        List<Investments> loanInvestmentInterests = new ArrayList<>();

        for (LoanInvestments loanInvestments : investments) {

            String fromDate = loanInvestments.getStartDate();
            String toDate = loanInvestments.getEndDate();

            if (isWithinRange(Date.valueOf(fromDate), startDate, endDate)
                    && isWithinRange(Date.valueOf(toDate), startDate, endDate)) {

                Investments investment = new Investments();

                investment.setInvestmentId(loanInvestments.getLenderId());
                investment.setInvestmentAmount(loanInvestments.getInitialAmount());

                long daysBetween = ChronoUnit.DAYS.between(LocalDate.parse(fromDate), LocalDate.parse(toDate));

                double interest = (interestRateDay * daysBetween) * loanInvestments.getInitialAmount();

                investment.setInterestOwed(Double.parseDouble(df2.format(interest)));

                loanInvestmentInterests.add(investment);
            }
        }

        investmentsInterest.setLoanInvestments(loanInvestmentInterests);

        return investmentsInterest;

    }

    private boolean isWithinRange(Date testDate, Date startDate, Date endDate) {
        return testDate.getTime() >= startDate.getTime() &&
                testDate.getTime() <= endDate.getTime();
    }

    @Autowired
    public VariableTimeLoanCalculationService(LoanInvestmentDetailsService loanInvestmentDetailsService) {
        this.loanInvestmentDetailsService = loanInvestmentDetailsService;
    }
}
