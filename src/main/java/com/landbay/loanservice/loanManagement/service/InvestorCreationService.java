package com.landbay.loanservice.loanManagement.service;

import com.landbay.loanservice.loanManagement.dao.InvestorDao;
import com.landbay.loanservice.loanManagement.domain.InvestorPayload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service
public class InvestorCreationService {

    InvestorDao investorDao;

    public int createInvestor(InvestorPayload investorPayload) {

        String lenderName = investorPayload.getLenderName();
        if(!Pattern.matches("^([a-zA-Z]{2,}\\s[a-zA-z]{1,}'?-?[a-zA-Z]{2,}\\s?([a-zA-Z]{1,})?)", lenderName))
            throw new IllegalArgumentException("Invalid value passed for field 'lenderName', value must only contain letters");
        investorPayload.setLenderName(lenderName);

        String lenderAddress = investorPayload.getLenderAddress();
        if(!Pattern.matches("^[#.0-9a-zA-Z\\s,-]+$", lenderAddress))
            throw  new IllegalArgumentException("Invalid value passed for field 'lenderAddress', value must match ^[#.0-9a-zA-Z\\s,-]+$");
        investorPayload.setLenderAddress(lenderAddress);

        int id = investorDao.createInvestor(investorPayload);

        return id;
    }

    @Autowired
    public InvestorCreationService(InvestorDao investorDao) {
        this.investorDao = investorDao;
    }
}
