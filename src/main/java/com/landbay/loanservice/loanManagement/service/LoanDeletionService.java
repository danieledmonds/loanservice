package com.landbay.loanservice.loanManagement.service;

import com.landbay.loanservice.loanManagement.dao.InvestmentDao;
import com.landbay.loanservice.loanManagement.dao.LoanDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoanDeletionService {

    InvestmentDao investmentDao;
    LoanDao loanDao;

    public boolean deleteLoan(int loanId) throws IllegalArgumentException {

        boolean investmentDeleted = investmentDao.deleteInvestment(loanId);

        boolean loanDeleted = loanDao.deleteLoan(loanId);

        return (investmentDeleted && loanDeleted);
    }

    @Autowired
    public LoanDeletionService(InvestmentDao investmentDao, LoanDao loanDao) {
        this.investmentDao = investmentDao;
        this.loanDao = loanDao;
    }
}
