package com.landbay.loanservice.loanManagement.service;

import com.landbay.loanservice.loanManagement.dao.LoanDao;
import com.landbay.loanservice.loanManagement.domain.Loan;
import com.landbay.loanservice.loanManagement.domain.LoanPayload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service
public class LoanCreationService {

    LoanDao loanDao;

    public int createLoan(LoanPayload loanPayload) throws IllegalArgumentException {

        Loan loan = new Loan();
        loan.setLoanStatus("PENDING_INVESTMENT");

        double loanValue = loanPayload.getLoanValue();
        if (loanValue < 0)
            throw new IllegalArgumentException("Invalid value passed for field 'loanValue', value must be positive");
        loan.setLoanValue(loanValue);

        double interestRate = loanPayload.getInterestRate();
        if (interestRate <= 0 || interestRate >= 100)
            throw new IllegalArgumentException("Invalid value passed for field 'interestRate', value must be positive");
        loan.setInterestRate(interestRate);

        String loaneeName = loanPayload.getLoaneeName();
        if(!Pattern.matches("^([a-zA-Z]{2,}\\s[a-zA-z]{1,}'?-?[a-zA-Z]{2,}\\s?([a-zA-Z]{1,})?)", loaneeName))
            throw new IllegalArgumentException("Invalid value passed for field 'loaneeName', value must only contain letters");
        loan.setLoaneeName(loaneeName);

        String loaneeAddress = loanPayload.getLoaneeAddress();
        if(!Pattern.matches("^[#.0-9a-zA-Z\\s,-]+$", loaneeAddress))
            throw  new IllegalArgumentException("Invalid value passed for field 'loaneeAddress', value must only contain letters and numbers");
        loan.setLoaneeAddress(loaneeAddress);

        loan.setStartingDatetime(java.sql.Date.valueOf(java.time.LocalDate.now()));

        return loanDao.createLoan(loan);
    }

    @Autowired
    public LoanCreationService(LoanDao loanDao) {
        this.loanDao = loanDao;
    }
}
