package com.landbay.loanservice.loanManagement.service;

import com.landbay.loanservice.loanManagement.dao.LoanDao;
import com.landbay.loanservice.loanManagement.dao.LoanInvestmentDao;
import com.landbay.loanservice.loanManagement.domain.Loan;
import com.landbay.loanservice.loanManagement.domain.LoanInvestments;
import com.landbay.loanservice.loanManagement.domain.LoanInvestmentsPayload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LoanInvestmentDetailsService{

    LoanInvestmentDao loanInvestmentDao;
    LoanDao loanDao;

    public LoanInvestmentsPayload retrieveLoan(int loanId) {

        //Validate that the loan exists and is OPEN_FOR_INVESTMENT
        if(!loanDao.verifyLoanId(loanId))
            throw  new IllegalArgumentException("No loan was found with Id: " + loanId);


        Loan loan = loanDao.fetchLoan(loanId);

        List<LoanInvestments> loanInvestments = loanInvestmentDao.retrieveLoanInvestments(loanId);

        LoanInvestmentsPayload loanInvestmentsPayload = new LoanInvestmentsPayload();
        loanInvestmentsPayload.setLoanId(loan.getLoanId());
        loanInvestmentsPayload.setLoanStatus(loan.getLoanStatus());
        loanInvestmentsPayload.setLoanValue(loan.getLoanValue());
        loanInvestmentsPayload.setRepaidAmount(loan.getRepaidAmount());
        loanInvestmentsPayload.setInterestRate(loan.getInterestRate());
        loanInvestmentsPayload.setStartingDatetime(loan.getStartingDateTime());
        loanInvestmentsPayload.setLoaneeName(loan.getLoaneeName());
        loanInvestmentsPayload.setLoaneeAddress(loan.getLoaneeAddress());
        loanInvestmentsPayload.setLoanInvestments(loanInvestments);

        return loanInvestmentsPayload;
    }

    @Autowired
    public LoanInvestmentDetailsService(LoanDao loanDao, LoanInvestmentDao loanInvestmentDao) {
        this.loanInvestmentDao = loanInvestmentDao;
        this.loanDao = loanDao;
    }
}
