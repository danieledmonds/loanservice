package com.landbay.loanservice.loanManagement.service;

import com.landbay.loanservice.loanManagement.dao.InvestmentDao;
import com.landbay.loanservice.loanManagement.dao.InvestorDao;
import com.landbay.loanservice.loanManagement.dao.LoanDao;
import com.landbay.loanservice.loanManagement.domain.InvestmentPayload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service
public class LoanInvestmentService {

    InvestorDao investorDao;
    InvestmentDao investmentDao;
    LoanDao loanDao;

    public boolean createInvestment(InvestmentPayload investmentPayload) throws IllegalArgumentException {

        //Validate that the loan exists and is OPEN_FOR_INVESTMENT
        int loanId = investmentPayload.getLoanId();
        if(!loanDao.verifyLoanId(loanId))
            throw  new IllegalArgumentException("No loan was found with Id: " + loanId);

        //Verify that investor exists
        int lenderId = investmentPayload.getLenderId();
        if(!investorDao.verifyLenderExists(lenderId))
            throw new IllegalArgumentException("No investor was found with Id: " + lenderId);

        //Verify start and end date
        String startDate = investmentPayload.getStartDate();
        if(!Pattern.matches("^\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12][0-9]|3[01])$", startDate))
            throw new IllegalArgumentException("Invalid value passed for field 'startDate', value must be in the format YYYY-MM-DD");

        String endDate = investmentPayload.getEndDate();
        if(!Pattern.matches("^\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12][0-9]|3[01])$", endDate))
            throw new IllegalArgumentException("Invalid value passed for field 'endDate', value must be in the format YYYY-MM-DD");

        //Validate investment amount
        double investmentAmount = investmentPayload.getInvestmentAmount();
        if(investmentAmount <= 0)
            throw new IllegalArgumentException("Investment amount must be greater than 0");

        //Create relationship between loan and investor
        return investmentDao.createInvestment(investmentPayload);

    }

    @Autowired
    public LoanInvestmentService(InvestorDao investorDao, InvestmentDao investmentDao, LoanDao loanDao) {
        this.investorDao = investorDao;
        this.investmentDao = investmentDao;
        this.loanDao = loanDao;
    }
}
