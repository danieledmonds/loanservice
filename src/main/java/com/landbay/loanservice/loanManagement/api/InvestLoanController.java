package com.landbay.loanservice.loanManagement.api;

import com.landbay.loanservice.loanManagement.domain.InvestmentPayload;
import com.landbay.loanservice.loanManagement.service.LoanInvestmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InvestLoanController {

    LoanInvestmentService loanInvestmentService;

    @RequestMapping(value = "/invest-loan", method = RequestMethod.POST)
    public ResponseEntity investLoan(@RequestBody InvestmentPayload investmentPayload) {
        try {
            boolean investmentId = loanInvestmentService.createInvestment(investmentPayload);
            if (investmentId) {
                return ResponseEntity.ok("Investment into loan " + investmentPayload.getLoanId() + " has been created");
            } throw new IllegalArgumentException("Failed to invest in load " + investmentPayload.getLoanId());
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Bad request: " + e.getLocalizedMessage());
        }
    }

    @Autowired
    public InvestLoanController(LoanInvestmentService loanInvestmentService) {
        this.loanInvestmentService = loanInvestmentService;
    }
}
