package com.landbay.loanservice.loanManagement.api;

import com.landbay.loanservice.loanManagement.service.LoanDeletionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class DeleteLoanController {

    LoanDeletionService loanDeletionService;

    @RequestMapping(value = "/delete-loan/{loanId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteLoan(@PathVariable("loanId") String loanId) {

        boolean response = loanDeletionService.deleteLoan(Integer.parseInt(loanId));

        if (response) return ResponseEntity.ok("Loan with ID " + loanId + " has successfully been deleted");
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Failed to delete a loan with ID " + loanId);
    }

    @Autowired
    public DeleteLoanController(LoanDeletionService loanDeletionService) {
        this.loanDeletionService = loanDeletionService;
    }
}
