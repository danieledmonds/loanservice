package com.landbay.loanservice.loanManagement.api;

import com.landbay.loanservice.loanManagement.domain.LoanInvestmentsPayload;
import com.landbay.loanservice.loanManagement.service.LoanInvestmentDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class RetrieveLoanController {

    LoanInvestmentDetailsService loanInvestmentDetailsService;

    @RequestMapping(value = "/retrieve-loan/{loanId}", method = RequestMethod.GET)
    public ResponseEntity retrieveLoan(@PathVariable("loanId") String loanId) {
        try {
            LoanInvestmentsPayload payload = loanInvestmentDetailsService.retrieveLoan(Integer.parseInt(loanId));
            if(payload != null)
                return ResponseEntity.ok(payload);
            throw new IllegalArgumentException("Unable to retrieve loan with ID: " + loanId);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getLocalizedMessage());
        }
    }

    @Autowired
    public RetrieveLoanController(LoanInvestmentDetailsService loanInvestmentDetailsService) {
        this.loanInvestmentDetailsService = loanInvestmentDetailsService;
    }

}
