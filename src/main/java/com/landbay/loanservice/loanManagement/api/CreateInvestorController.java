package com.landbay.loanservice.loanManagement.api;

import com.landbay.loanservice.loanManagement.domain.InvestorPayload;
import com.landbay.loanservice.loanManagement.service.InvestorCreationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CreateInvestorController {

    InvestorCreationService investorCreationService;

    @RequestMapping(value = "/create-investor", method = RequestMethod.POST)
    public ResponseEntity investLoan(@RequestBody InvestorPayload investorPayload) {

        try {
            int id = investorCreationService.createInvestor(investorPayload);
            return ResponseEntity.ok("Investor profile has been created with ID: " + id);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Bad request: " + e.getLocalizedMessage());
        }

    }

    @Autowired
    public CreateInvestorController(InvestorCreationService investorCreationService) {
        this.investorCreationService = investorCreationService;
    }
}
