package com.landbay.loanservice.loanManagement.api;


import com.landbay.loanservice.loanManagement.domain.LoanPayload;
import com.landbay.loanservice.loanManagement.service.LoanCreationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CreateLoanController {

    LoanCreationService loanCreationService;

    @RequestMapping(value = "/create-loan", method = RequestMethod.POST)
    public ResponseEntity createLoan(@RequestBody LoanPayload loanPayload) {
        try {
            int id = loanCreationService.createLoan(loanPayload);
            return ResponseEntity.ok("Your loan has been created with ID: " + id);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Bad request: " + e.getLocalizedMessage());
        }
    }

    @Autowired
    public CreateLoanController(LoanCreationService loanCreationService) {
        this.loanCreationService = loanCreationService;
    }
}
