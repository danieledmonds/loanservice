package com.landbay.loanservice.loanManagement.dao;

import com.landbay.loanservice.loanManagement.domain.InvestorPayload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

@Service
public class InvestorDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }


    public int createInvestor(InvestorPayload investor) {
        KeyHolder holder = new GeneratedKeyHolder();

        String SQL = "insert into lender (lender_name, lender_address) values (?, ?)";

        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, investor.getLenderName());
                ps.setString(2, investor.getLenderName());
                return ps;
            }
        }, holder);

        int investorId = holder.getKey().intValue();
        return investorId;
    }

    public boolean verifyLenderExists(int lenderId) {
        String sql = "SELECT count(*) FROM lender WHERE id = ? LIMIT 1";
        boolean result = false;
        int count = jdbcTemplate.queryForObject(sql, new Object[] { lenderId }, Integer.class);
        if (count > 0) {
            result = true;
        }
        return result;
    }
}
