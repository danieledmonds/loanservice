package com.landbay.loanservice.loanManagement.dao;

import com.landbay.loanservice.common.DateUtils;
import com.landbay.loanservice.loanManagement.domain.LoanInvestments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class LoanInvestmentDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<LoanInvestments> retrieveLoanInvestments(int investor) {

        String SQL = "SELECT ll.loan_id, ll.lender_id, ll.initial_amount, l.lender_name, l.lender_address, " +
                "ll.investment_start_date, ll.investment_end_date FROM loan_lender ll, lender l " +
                "WHERE ll.lender_id = l.id AND ll.loan_id = ?";

        List<LoanInvestments> loanInvestments = new ArrayList<LoanInvestments>();

        List<Map<String, Object>> rows = jdbcTemplate.queryForList(SQL, investor);
        for (Map row : rows) {
            LoanInvestments loanInvestment = new LoanInvestments();
            loanInvestment.setLenderId((Integer) (row.get("LENDER_ID")));
            loanInvestment.setInitialAmount((Double)row.get("INITIAL_AMOUNT"));
            loanInvestment.setLenderName((String) row.get("LENDER_NAME"));
            loanInvestment.setLenderAddress((String) row.get("LENDER_ADDRESS"));
            loanInvestment.setStartDate(DateUtils.toDateString(row.get("INVESTMENT_START_DATE")));
            loanInvestment.setEndDate(DateUtils.toDateString(row.get("INVESTMENT_END_DATE")));
            loanInvestments.add(loanInvestment);
        }
        return loanInvestments;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }

}
