package com.landbay.loanservice.loanManagement.dao;

import com.landbay.loanservice.common.DateUtils;
import com.landbay.loanservice.loanManagement.domain.InvestmentPayload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class InvestmentDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public boolean createInvestment(InvestmentPayload investor) {
        String SQL = "INSERT INTO loan_lender (loan_id, lender_id, initial_amount, investment_start_date, investment_end_date) VALUES (?, ?, ?, ?, ?)";

        try {
            int updated = jdbcTemplate.update(SQL, investor.getLoanId(), investor.getLenderId(), investor.getInvestmentAmount(), DateUtils.toSqlDate(investor.getStartDate()), DateUtils.toSqlDate(investor.getEndDate()));
            return (updated > 0);

        } catch (Exception e) {
            return false;
        }
    }

    public boolean deleteInvestment(int loanId){
        String SQL = "DELETE FROM loan_lender WHERE loan_id = ?";

        try {
            jdbcTemplate.update(SQL, loanId);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
}
