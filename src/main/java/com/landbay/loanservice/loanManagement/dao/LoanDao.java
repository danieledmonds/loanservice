package com.landbay.loanservice.loanManagement.dao;

import com.landbay.loanservice.loanManagement.domain.Loan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;

@Service
public class LoanDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }

//    public int createLoan(Loan loan) {
//        String SQL = "insert into loan (loan_status, loan_value, repaid_amount, interest_rate, starting_date, loanee_name, loanee_address) values (?, ?, ?, ?, ?, ?, ?)";
//        return jdbcTemplate.update(SQL, loan.getLoanStatus(), loan.getLoanValue(), loan.getRepaidAmount(), loan.getInterestRate(),
//                loan.getStartingDateTime(), loan.getLoaneeName(), loan.getLoaneeAddress());
//    }

    public int createLoan(Loan loan) {
        KeyHolder holder = new GeneratedKeyHolder();

        String SQL = "insert into loan (loan_status, loan_value, repaid_amount, interest_rate, starting_date, loanee_name, loanee_address) values (?, ?, ?, ?, ?, ?, ?)";

        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, loan.getLoanStatus());
                ps.setDouble(2, loan.getLoanValue());
                ps.setDouble(3, loan.getRepaidAmount());
                ps.setDouble(4, loan.getInterestRate());
                ps.setDate(5, loan.getStartingDateTime());
                ps.setString(6, loan.getLoaneeName());
                ps.setString(7, loan.getLoaneeAddress());
                return ps;
            }
        }, holder);

        int newUserId = holder.getKey().intValue();
        loan.setLoanId(newUserId);
        return loan.getLoanId();
    }


    public boolean verifyLoanId(int loanId) {
        String sql = "SELECT count(*) FROM loan WHERE id = ? LIMIT 1";
        boolean result = false;
        int count = jdbcTemplate.queryForObject(sql, new Object[] { loanId }, Integer.class);
        if (count > 0) {
            result = true;
        }
        return result;
    }

    public Loan fetchLoan(int loanId) {
        String SQL = "SELECT id, loan_status, loan_value, repaid_amount, interest_rate, starting_date, loanee_name, loanee_address FROM loan WHERE id = ?";

        List<Map<String, Object>> rows = jdbcTemplate.queryForList(SQL, loanId);
        Map<String, Object> row = rows.get(0);

        Loan loan = new Loan();
        loan.setLoanId((Integer) (row.get("ID")));
        loan.setLoanStatus((String)row.get("LOAN_STATUS"));
        loan.setLoanValue((Double) row.get("LOAN_VALUE"));
        loan.setRepaidAmount((Double) row.get("REPAID_AMOUNT"));
        loan.setInterestRate((Double) row.get("INTEREST_RATE"));
        loan.setStartingDatetime((java.sql.Date) row.get("STARTING_DATE"));
        loan.setLoaneeName((String) row.get("LOANEE_NAME"));
        loan.setLoaneeAddress((String) row.get("LOANEE_ADDRESS"));

        return loan;
    }

    public boolean deleteLoan(int loanId){
        String SQL = "DELETE FROM loan WHERE id = ?";

        try {
            int updated = jdbcTemplate.update(SQL, loanId);
            return (updated > 0);
        } catch (Exception e) {
            return false;
        }
    }
}
