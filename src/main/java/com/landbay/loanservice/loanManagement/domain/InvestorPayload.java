package com.landbay.loanservice.loanManagement.domain;

public class InvestorPayload {

    private String lenderName;
    private String lenderAddress;

    public String getLenderName(){
        return lenderName;
    }

    public void setLenderName(String lenderName){
        this.lenderName = lenderName;
    }

    public String getLenderAddress(){
        return lenderAddress;
    }

    public void setLenderAddress(String lenderAddress){
        this.lenderAddress = lenderAddress;
    }

}
