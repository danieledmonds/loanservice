package com.landbay.loanservice.loanManagement.domain;

public class LoanPayload {

    private double loanValue;
    private double interestRate;
    private String loaneeName;
    private String loaneeAddress;


    public void setLoanValue(double loanValue){
        this.loanValue = loanValue;
    }

    public double getLoanValue(){
        return loanValue;
    }

    public void setInterestRate(double interestRate){
        this.interestRate = interestRate;
    }

    public double getInterestRate(){
        return interestRate;
    }

    public void setLoaneeName(String loaneeName) { this.loaneeName = loaneeName; }

    public String getLoaneeName() {
        return loaneeName;
    }

    public void setLoaneeAddress(String loaneeAddress) { this.loaneeAddress = loaneeAddress; }

    public String getLoaneeAddress(){
        return loaneeAddress;
    }
}
