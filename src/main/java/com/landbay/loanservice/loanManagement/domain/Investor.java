package com.landbay.loanservice.loanManagement.domain;

public class Investor {

    private int lenderId;
    private String lenderName;
    private String lenderAddress;

    public int getLenderId(){
        return lenderId;
    }

    public void setLenderId(int lenderId){
        this.lenderId = lenderId;
    }

    public String getLenderName(){
        return lenderName;
    }

    public void setLenderName(String lenderName){
        this.lenderName = lenderName;
    }

    public String getLenderAddress(){
        return lenderAddress;
    }

    public void setLenderAddress(String lenderAddress){
        this.lenderAddress = lenderAddress;
    }

}
