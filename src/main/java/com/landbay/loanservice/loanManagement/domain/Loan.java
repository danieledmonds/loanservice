package com.landbay.loanservice.loanManagement.domain;

public class Loan {

    private int loanId;
    private String loanStatus;
    private double loanValue;
    private double repaidAmount;
    private double interestRate;
    private java.sql.Date startingDateTime;
    private String loaneeName;
    private String loaneeAddress;

    public void setLoanId(int loanId){
        this.loanId = loanId;
    }

    public int getLoanId(){
        return loanId;
    }

    public void setLoanStatus(String loanStatus){
        this.loanStatus = loanStatus;
    }

    public String getLoanStatus(){
        return loanStatus;
    }

    public void setLoanValue(double loanValue){
        this.loanValue = loanValue;
    }

    public double getLoanValue(){
        return loanValue;
    }

    public void setRepaidAmount(double repaidAmount){
        this.repaidAmount = repaidAmount;
    }

    public double getRepaidAmount(){
        return repaidAmount;
    }

    public void setInterestRate(double interestRate){
        this.interestRate = interestRate;
    }

    public double getInterestRate(){
        return interestRate;
    }

    public void setStartingDatetime(java.sql.Date startingDateTime){
        this.startingDateTime = startingDateTime;
    }

    public java.sql.Date getStartingDateTime(){
        return startingDateTime;
    }


    public void setLoaneeName(String loaneeName) { this.loaneeName = loaneeName; }

    public String getLoaneeName() {
        return loaneeName;
    }

    public void setLoaneeAddress(String loaneeAddress) { this.loaneeAddress = loaneeAddress; }

    public String getLoaneeAddress(){
        return loaneeAddress;
    }

}
