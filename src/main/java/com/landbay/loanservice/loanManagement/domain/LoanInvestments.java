package com.landbay.loanservice.loanManagement.domain;

public class LoanInvestments {

    private int lenderId;
    private double initialAmount;
    private String lenderName;
    private String lenderAddress;
    private String startDate;
    private String endDate;



    public int getLenderId(){
        return lenderId;
    }

    public void setLenderId(int lenderId){
        this.lenderId = lenderId;
    }

    public double getInitialAmount(){
        return  initialAmount;
    }

    public void setInitialAmount(double initialAmount){
        this.initialAmount = initialAmount;
    }

    public String getLenderName(){
        return lenderName;
    }

    public void setLenderName(String lenderName){
        this.lenderName = lenderName;
    }

    public String getLenderAddress(){
        return lenderAddress;
    }

    public void setLenderAddress(String lenderAddress){
        this.lenderAddress = lenderAddress;
    }

    public String getStartDate() { return startDate; }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() { return endDate; }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
