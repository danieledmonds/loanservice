package com.landbay.loanservice.loanManagement.domain;

public class InvestmentPayload {

    private int lenderId;
    private double investmentAmount;
    private int loanId;
    private String startDate;
    private String endDate;

    public int getLenderId(){ return lenderId; }

    public void setLenderId(int lenderId){
        this.lenderId = lenderId;
    }

    public double getInvestmentAmount(){
        return investmentAmount;
    }

    public void setInvestmentAmount(double investmentAmount){
        this.investmentAmount = investmentAmount;
    }

    public int getLoanId(){
        return loanId;
    }

    public void setLoanId(int loanId){
        this.loanId = loanId;
    }

    public String getStartDate() { return startDate; }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() { return endDate; }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
