package com.landbay.loanservice.common;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateUtils {

    private static String DATE_FORMAT = "yyyy-MM-dd";

    /**
     * Converts yyyy-dd-mm format date string to java.sql.Date
     * @param dateString
     * @return java.sql.Date
     * @throws ParseException
     */
    public static java.sql.Date toSqlDate(String dateString) throws ParseException {
        SimpleDateFormat sdf1 = new SimpleDateFormat(DATE_FORMAT);
        java.util.Date date = sdf1.parse(dateString);
        return new java.sql.Date(date.getTime());
    }

    /**
     * Used in sql row mapping to convert object to date string
     * @param date
     * @return String
     * @throws ParseException
     */
    public static String toDateString(Object date){
        DateFormat df = new SimpleDateFormat(DATE_FORMAT);
        return df.format(date);
    }
}
