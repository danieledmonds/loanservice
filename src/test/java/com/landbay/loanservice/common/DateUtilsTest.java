package com.landbay.loanservice.common;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Date;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class DateUtilsTest {

    @Test
    public void convertDateStringToSqlDateSuccessTest() throws Exception {

        Date date = DateUtils.toSqlDate("1970-01-01");

        Date expected = new Date(1);

      /*java.sql.Date adds additional character on the end,
        therefore converting back to string to verify*/
        assertEquals(expected.toString(), date.toString());
    }

    @Test
    public void convertObjectToDateString() throws Exception {

        String date = DateUtils.toDateString((Object) new Date(1));

        assertEquals("1970-01-01", date);
    }

    @Test
    public void convertObjectToDateStringNull() throws Exception {

        String exception = null;
        try {
            String date = DateUtils.toDateString((Object) null);
        } catch (IllegalArgumentException e) {
            exception = e.getMessage();
        }
        assertEquals("Cannot format given Object as a Date", exception);
    }
}
