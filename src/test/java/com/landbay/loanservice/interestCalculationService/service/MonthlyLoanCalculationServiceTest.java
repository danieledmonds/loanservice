package com.landbay.loanservice.interestCalculationService.service;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.landbay.loanservice.interestCalculationService.domain.InvestmentsInterest;
import com.landbay.loanservice.loanManagement.domain.LoanInvestments;
import com.landbay.loanservice.loanManagement.domain.LoanInvestmentsPayload;
import com.landbay.loanservice.loanManagement.service.LoanInvestmentDetailsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class MonthlyLoanCalculationServiceTest {

    @Mock
    LoanInvestmentDetailsService loanInvestmentDetailsService;

    @Test
    public void createLoanInvestmentTestSuccessfully() throws Exception {

        LoanInvestments li = new LoanInvestments();
        li.setLenderAddress("sd");
        li.setInitialAmount(10000);
        li.setLenderName("Dave");

        List<LoanInvestments> loanInvestmentsList = new ArrayList<>();
        loanInvestmentsList.add(li);

        LoanInvestmentsPayload loanInvestmentsPayload = new LoanInvestmentsPayload();
        loanInvestmentsPayload.setLoanId(123);
        loanInvestmentsPayload.setLoanStatus("ACTIVE");
        loanInvestmentsPayload.setLoanValue(100000.00);
        loanInvestmentsPayload.setRepaidAmount(0.0);
        loanInvestmentsPayload.setInterestRate(1.5);
        loanInvestmentsPayload.setStartingDatetime(new Date(11111));
        loanInvestmentsPayload.setLoaneeName("Daniel");
        loanInvestmentsPayload.setLoaneeAddress("sdsdsd");
        loanInvestmentsPayload.setLoanInvestments(loanInvestmentsList);

        MonthLoanCalculationService monthLoanCalculationService = new MonthLoanCalculationService(loanInvestmentDetailsService);

        Mockito.when(loanInvestmentDetailsService.retrieveLoan(1)).thenReturn(loanInvestmentsPayload);

        InvestmentsInterest response = monthLoanCalculationService.calculateLoan(1);

        assertEquals( expectedResponse(), objectToJson(response));
    }

    private String expectedResponse(){
        return "{\"loanId\":1,\"loanValue\":100000.0,\"loanInvestments\":[{\"investmentId\":0,\"investmentAmount\":10000.0,\"interestOwed\":12.5}]}";
    }


    private String objectToJson(Object object){
        ObjectMapper mapper = new ObjectMapper();
        String json = null;
        try {
            json = mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return json;
    }

}
