package com.landbay.loanservice.interestCalculationService.api;


import com.landbay.loanservice.interestCalculationService.domain.Investments;
import com.landbay.loanservice.interestCalculationService.domain.InvestmentsInterest;
import com.landbay.loanservice.interestCalculationService.service.MonthLoanCalculationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class MonthInterestCalculationApiTest {

    @Mock
    MonthLoanCalculationService monthLoanCalculationService;

    @Test
    public void createLoanInvestmentTestSuccessfully() throws Exception {

        InvestmentsInterest investmentsInterest = new InvestmentsInterest();
        investmentsInterest.setLoanId(3);
        investmentsInterest.setLoanValue(3000.00);

        List<Investments> loanInvestmentInterests = new ArrayList<>();

        Investments investment1 = new Investments();
        investment1.setInvestmentId(1);
        investment1.setInvestmentAmount(1000.00);
        investment1.setInterestOwed(500.00);
        loanInvestmentInterests.add(investment1);

        Investments investment2 = new Investments();
        investment2.setInvestmentId(2);
        investment2.setInvestmentAmount(2000.00);
        investment2.setInterestOwed(1000.00);
        loanInvestmentInterests.add(investment2);

        investmentsInterest.setLoanInvestments(loanInvestmentInterests);



        MonthInterestCalculationApi ionthInterestCalculationApi = new MonthInterestCalculationApi(monthLoanCalculationService);

        Mockito.when(monthLoanCalculationService.calculateLoan(3)).thenReturn(investmentsInterest);

        ResponseEntity response = ionthInterestCalculationApi.calculateInterest("3");

        assertEquals(ResponseEntity.ok(investmentsInterest), response);
    }

    @Test
    public void loanInvestmentTestRespondsWithError() throws Exception {

        MonthInterestCalculationApi ionthInterestCalculationApi = new MonthInterestCalculationApi(monthLoanCalculationService);

        Mockito.when(monthLoanCalculationService.calculateLoan(3)).thenThrow(new IllegalArgumentException("null"));

        ResponseEntity response = ionthInterestCalculationApi.calculateInterest("3");

        assertEquals(ResponseEntity.status(HttpStatus.BAD_REQUEST).body("An error occurred while calculating interest"), response);
    }

}
