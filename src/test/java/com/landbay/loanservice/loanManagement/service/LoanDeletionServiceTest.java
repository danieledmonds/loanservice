package com.landbay.loanservice.loanManagement.service;


import com.landbay.loanservice.loanManagement.dao.InvestmentDao;
import com.landbay.loanservice.loanManagement.dao.LoanDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class LoanDeletionServiceTest {

    @Mock
    LoanDao loanDao;

    @Mock
    InvestmentDao investmentDao;

    @Test
    public void deleteLoanAndInvestmentsSucceeds() throws Exception {

        LoanDeletionService loanDeletionService = new LoanDeletionService(investmentDao, loanDao);

        Mockito.when(investmentDao.deleteInvestment(1)).thenReturn(true);
        Mockito.when(loanDao.deleteLoan(1)).thenReturn(true);

        boolean response = loanDeletionService.deleteLoan(1);

        assertEquals(true, response);
    }

    @Test
    public void deleteLoanAndInvestmentsFails() throws Exception {

        LoanDeletionService loanDeletionService = new LoanDeletionService(investmentDao, loanDao);

        Mockito.when(investmentDao.deleteInvestment(1)).thenReturn(false);
        Mockito.when(loanDao.deleteLoan(1)).thenReturn(true);

        boolean response = loanDeletionService.deleteLoan(1);

        assertEquals(false, response);
    }

}
