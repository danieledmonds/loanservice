package com.landbay.loanservice.loanManagement.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.landbay.loanservice.loanManagement.dao.LoanDao;
import com.landbay.loanservice.loanManagement.dao.LoanInvestmentDao;
import com.landbay.loanservice.loanManagement.domain.Loan;
import com.landbay.loanservice.loanManagement.domain.LoanInvestments;
import com.landbay.loanservice.loanManagement.domain.LoanInvestmentsPayload;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class LoanInvestmentDetailsServiceTest {

    @Mock
    LoanInvestmentDao loanInvestmentDao;

    @Mock
    LoanDao loanDao;

    @Test
    public void deleteLoanAndInvestmentsSucceeds() throws Exception {

        LoanInvestmentDetailsService loanInvestmentDetailsService = new LoanInvestmentDetailsService(loanDao, loanInvestmentDao);

        Loan loan = new Loan();
        loan.setLoaneeAddress("5 Alpha Street, CF426FS");
        loan.setLoaneeName("John Doe");
        loan.setStartingDatetime(new Date(111));
        loan.setInterestRate(2.5);
        loan.setRepaidAmount(1000.00);
        loan.setLoanValue(1000.00);
        loan.setLoanStatus("ACTIVE");
        loan.setLoanId(1);


        LoanInvestments loanInvestments = new LoanInvestments();
        loanInvestments.setLenderName("Dave Jones");
        loanInvestments.setInitialAmount(1000.00);
        loanInvestments.setLenderAddress("10 Downing Street, WD333FL");
        loanInvestments.setLenderId(1);
        loanInvestments.setStartDate("1970-01-01");
        loanInvestments.setEndDate("1970-01-02");
        List<LoanInvestments> investments = new ArrayList<>();
        investments.add(loanInvestments);

        Mockito.when(loanDao.verifyLoanId(1)).thenReturn(true);
        Mockito.when(loanDao.fetchLoan(1)).thenReturn(loan);
        Mockito.when(loanInvestmentDao.retrieveLoanInvestments(1)).thenReturn(investments);

        LoanInvestmentsPayload response = loanInvestmentDetailsService.retrieveLoan(1);

        assertEquals(expected(), objectToJson(response));

    }

    private String objectToJson(Object object){
        ObjectMapper mapper = new ObjectMapper();
        String json = null;
        try {
            json = mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return json;
    }

    private String expected(){
        return "{\"loanId\":1,\"loanStatus\":\"ACTIVE\",\"loanValue\":1000.0,\"repaidAmount\":1000.0,\"interestRate\":2.5,\"startingDateTime\":111,\"loaneeName\":\"John Doe\",\"loaneeAddress\":\"5 Alpha Street, CF426FS\",\"loanInvestments\":[{\"lenderId\":1,\"initialAmount\":1000.0,\"lenderName\":\"Dave Jones\",\"lenderAddress\":\"10 Downing Street, WD333FL\",\"startDate\":\"1970-01-01\",\"endDate\":\"1970-01-02\"}]}";
    }
}
