package com.landbay.loanservice.loanManagement.service;


import com.landbay.loanservice.loanManagement.dao.InvestmentDao;
import com.landbay.loanservice.loanManagement.dao.InvestorDao;
import com.landbay.loanservice.loanManagement.dao.LoanDao;
import com.landbay.loanservice.loanManagement.domain.InvestmentPayload;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class LoanInvestmentSericeTest {

    @Mock
    InvestmentDao investmentDao;

    @Mock
    InvestorDao investorDao;

    @Mock
    LoanDao loanDao;

    @Test
    public void createLoanPassesThroughChecks() throws Exception {

        InvestmentPayload i = new InvestmentPayload();
        i.setInvestmentAmount(1000.00);
        i.setLoanId(2);
        i.setLenderId(1);
        i.setStartDate("2012-10-9");
        i.setEndDate("2018-9-5");

        LoanInvestmentService loanInvestmentService = new LoanInvestmentService(investorDao, investmentDao, loanDao);

        Mockito.when(loanDao.verifyLoanId(i.getLoanId())).thenReturn(true);
        Mockito.when(investorDao.verifyLenderExists(i.getLenderId())).thenReturn(true);
        Mockito.when(investmentDao.createInvestment(i)).thenReturn(true);

        boolean response = loanInvestmentService.createInvestment(i);

        assertEquals(true, response);
    }

    @Test
    public void createLoanFailsDueToNoLoanBeingFound() throws Exception {

        InvestmentPayload i = new InvestmentPayload();
        i.setInvestmentAmount(1000.00);
        i.setLoanId(2);
        i.setLenderId(1);
        i.setStartDate("2012-10-9");
        i.setEndDate("2018-9-5");

        LoanInvestmentService loanInvestmentService = new LoanInvestmentService(investorDao, investmentDao, loanDao);

        Mockito.when(loanDao.verifyLoanId(i.getLoanId())).thenReturn(false);
        Mockito.when(investorDao.verifyLenderExists(i.getLenderId())).thenReturn(true);
        Mockito.when(investmentDao.createInvestment(i)).thenReturn(true);

        String exception = null;

        try {
            boolean response = loanInvestmentService.createInvestment(i);
        }catch (IllegalArgumentException e){
            exception = e.getLocalizedMessage();
        }
        assertEquals("No loan was found with Id: 2", exception);
    }

}
