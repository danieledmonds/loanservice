package com.landbay.loanservice.loanManagement.service;


import com.landbay.loanservice.loanManagement.dao.LoanDao;
import com.landbay.loanservice.loanManagement.domain.LoanPayload;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class LoanCreationServiceTest {

    @Mock
    LoanDao loanDao;

    @Test
    public void createLoanPassesThroughChecks() throws Exception {

        LoanPayload lp = new LoanPayload();
        lp.setLoanValue(100000.00);
        lp.setInterestRate(2.5);
        lp.setLoaneeAddress("5address");
        lp.setLoaneeName("John Doe");

        LoanCreationService loanCreationService = new LoanCreationService(loanDao);

        Mockito.when(loanDao.createLoan(anyObject())).thenReturn(1);

        int response = loanCreationService.createLoan(lp);

        assertEquals(1, response);
    }

    @Test
    public void createLoanFailsWithIllegalArgumentException() throws Exception {

        LoanPayload lp = new LoanPayload();
        lp.setLoanValue(100000.00);
        lp.setInterestRate(2.5);
        lp.setLoaneeAddress("3jj3j3");
        lp.setLoaneeName("999999999999");

        LoanCreationService loanCreationService = new LoanCreationService(loanDao);

        Mockito.when(loanDao.createLoan(anyObject())).thenReturn(1);

        String response = null;

        try {
            int a = loanCreationService.createLoan(lp);
        } catch (IllegalArgumentException e) {
            response = e.getLocalizedMessage();
        }

        assertEquals("Invalid value passed for field 'loaneeName', value must only contain letters", response);
    }

}
