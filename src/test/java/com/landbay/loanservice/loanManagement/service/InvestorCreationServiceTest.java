package com.landbay.loanservice.loanManagement.service;


import com.landbay.loanservice.loanManagement.dao.InvestorDao;
import com.landbay.loanservice.loanManagement.domain.InvestorPayload;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.internal.matchers.Equality.areEqual;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class InvestorCreationServiceTest {

    @Mock
    InvestorDao investorDao;

    @Test
    public void createInvestorSucceeds() throws Exception {

        InvestorPayload investorPayload = new InvestorPayload();
        investorPayload.setLenderAddress("5, Alpha Terrace, CF347PL");
        investorPayload.setLenderName("Daniel Edmonds");

        InvestorCreationService lnvestorCreationService = new InvestorCreationService(investorDao);

        Mockito.when(investorDao.createInvestor(anyObject())).thenReturn(1);

        int response = lnvestorCreationService.createInvestor(investorPayload);

        assertEquals(1, response);
    }

    @Test
    public void createInvestorFailsAddressValidation() throws Exception {

        InvestorPayload investorPayload = new InvestorPayload();
        investorPayload.setLenderAddress("5, %$£Alpha Terrace, CF347PL");
        investorPayload.setLenderName("Daniel Edmonds");

        InvestorCreationService lnvestorCreationService = new InvestorCreationService(investorDao);

        Mockito.when(investorDao.createInvestor(anyObject())).thenReturn(1);

        IllegalArgumentException exception = null;

        try {

            int response = lnvestorCreationService.createInvestor(investorPayload);

        } catch (IllegalArgumentException e){
            exception = e;
        }

        areEqual(new IllegalArgumentException("Invalid value passed for field 'lenderAddress', value must match ^[#.0-9a-zA-Z\\s,-]+$"), exception);
    }

    @Test
    public void createInvestorFailsNameValidation() throws Exception {

        InvestorPayload investorPayload = new InvestorPayload();
        investorPayload.setLenderAddress("5, Alpha Terrace, CF347PL");
        investorPayload.setLenderName("Daniel&$%");

        InvestorCreationService lnvestorCreationService = new InvestorCreationService(investorDao);

        Mockito.when(investorDao.createInvestor(anyObject())).thenReturn(1);

        IllegalArgumentException exception = null;

        try {

            int response = lnvestorCreationService.createInvestor(investorPayload);

        } catch (IllegalArgumentException e){
            exception = e;
        }
        areEqual(new IllegalArgumentException("Invalid value passed for field 'lenderName', value must only contain letters"), exception);
    }

}
