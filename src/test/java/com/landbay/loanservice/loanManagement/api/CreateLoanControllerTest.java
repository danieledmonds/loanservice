package com.landbay.loanservice.loanManagement.api;


import com.landbay.loanservice.loanManagement.service.LoanCreationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class CreateLoanControllerTest {

    @Mock
    LoanCreationService loanCreationService;

    @Test
    public void createLoanTestSuccessfully() throws Exception {

        CreateLoanController createLoanController = new CreateLoanController(loanCreationService);

        Mockito.when(loanCreationService.createLoan(anyObject())).thenReturn(1);

        ResponseEntity response = createLoanController.createLoan(anyObject());

        assertEquals(ResponseEntity.ok("Your loan has been created with ID: 1"), response);
    }

    @Test
    public void createLoanTestRespondsWithError() throws Exception {

        CreateLoanController createLoanController = new CreateLoanController(loanCreationService);

        Mockito.when(loanCreationService.createLoan(anyObject())).thenThrow(new IllegalArgumentException("null"));

        ResponseEntity response = createLoanController.createLoan(anyObject());

        assertEquals(ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Bad request: null"), response);
    }


}
