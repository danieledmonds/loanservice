package com.landbay.loanservice.loanManagement.api;


import com.landbay.loanservice.loanManagement.service.LoanDeletionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class DeleteLoanControllerTest {

    @Mock
    LoanDeletionService loanDeletionService;

    @Test
    public void deleteLoanInvestmentTestSuccessfully() throws Exception {

        DeleteLoanController deleteLoanController = new DeleteLoanController(loanDeletionService);

        Mockito.when(loanDeletionService.deleteLoan(23)).thenReturn(true);

        ResponseEntity response = deleteLoanController.deleteLoan("23");

        assertEquals(ResponseEntity.ok("Loan with ID 23 has successfully been deleted"), response);
    }

    @Test
    public void deleteInvestmentTestRespondsWithError() throws Exception {

        DeleteLoanController deleteLoanController = new DeleteLoanController(loanDeletionService);

        Mockito.when(loanDeletionService.deleteLoan(23)).thenReturn(false);

        ResponseEntity response = deleteLoanController.deleteLoan("23");

        assertEquals(ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Failed to delete a loan with ID 23"), response);
    }

}
