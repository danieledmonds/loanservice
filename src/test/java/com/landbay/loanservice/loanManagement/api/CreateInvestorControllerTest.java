package com.landbay.loanservice.loanManagement.api;


import com.landbay.loanservice.loanManagement.service.InvestorCreationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class CreateInvestorControllerTest {

    @Mock
    InvestorCreationService investorCreationService;

    @Test
    public void createLoanTestSuccessfully() throws Exception {

        CreateInvestorController createInvestorController = new CreateInvestorController(investorCreationService);

        Mockito.when(investorCreationService.createInvestor(anyObject())).thenReturn(1);

        ResponseEntity response = createInvestorController.investLoan(anyObject());

        assertEquals(ResponseEntity.ok("Investor profile has been created with ID: 1"), response);
    }

    @Test
    public void createLoanTestRespondsWithError() throws Exception {

        CreateInvestorController createInvestorController = new CreateInvestorController(investorCreationService);

        Mockito.when(investorCreationService.createInvestor(anyObject())).thenThrow(new IllegalArgumentException("null"));

        ResponseEntity response = createInvestorController.investLoan(anyObject());

        assertEquals(ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Bad request: null"), response);
    }

}
