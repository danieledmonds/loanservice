package com.landbay.loanservice.loanManagement.api;


import com.landbay.loanservice.loanManagement.domain.LoanInvestments;
import com.landbay.loanservice.loanManagement.domain.LoanInvestmentsPayload;
import com.landbay.loanservice.loanManagement.service.LoanInvestmentDetailsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class RetrieveLoanControllerTest {

    @Mock
    LoanInvestmentDetailsService loanInvestmentDetailsService;

    @Test
    public void createLoanInvestmentTestSuccessfully() throws Exception {

        LoanInvestments li = new LoanInvestments();
        li.setLenderAddress("sd");
        li.setInitialAmount(100);
        li.setLenderName("Dave");

        List<LoanInvestments> loanInvestmentsList = new ArrayList<>();
        loanInvestmentsList.add(li);

        LoanInvestmentsPayload loanInvestmentsPayload = new LoanInvestmentsPayload();
        loanInvestmentsPayload.setLoanId(123);
        loanInvestmentsPayload.setLoanStatus("ACTIVE");
        loanInvestmentsPayload.setLoanValue(1000.00);
        loanInvestmentsPayload.setRepaidAmount(0.0);
        loanInvestmentsPayload.setInterestRate(1.5);
        loanInvestmentsPayload.setStartingDatetime(new Date(11111));
        loanInvestmentsPayload.setLoaneeName("Daniel");
        loanInvestmentsPayload.setLoaneeAddress("sdsdsd");
        loanInvestmentsPayload.setLoanInvestments(loanInvestmentsList);


        RetrieveLoanController retrieveLoanController = new RetrieveLoanController(loanInvestmentDetailsService);

        Mockito.when(loanInvestmentDetailsService.retrieveLoan(23)).thenReturn(loanInvestmentsPayload);

        ResponseEntity response = retrieveLoanController.retrieveLoan("23");

        assertEquals(ResponseEntity.ok(loanInvestmentsPayload), response);
    }

    @Test
    public void loanInvestmentTestRespondsWithError() throws Exception {



        RetrieveLoanController retrieveLoanController = new RetrieveLoanController(loanInvestmentDetailsService);

        Mockito.when(loanInvestmentDetailsService.retrieveLoan(23)).thenReturn(null);

        ResponseEntity response = retrieveLoanController.retrieveLoan("23");

        assertEquals(ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Unable to retrieve loan with ID: 23"), response);
    }

}
