package com.landbay.loanservice.loanManagement.api;


import com.landbay.loanservice.loanManagement.domain.InvestmentPayload;
import com.landbay.loanservice.loanManagement.service.LoanInvestmentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class CreateInvestmentControllerTest {

    @Mock
    LoanInvestmentService loanInvestmentService;

    @Test
    public void createLoanInvestmentTestSuccessfully() throws Exception {

        InvestmentPayload p = new InvestmentPayload();
        p.setLenderId(1);
        p.setLoanId(2);
        p.setInvestmentAmount(3.3);

        InvestLoanController investLoanController = new InvestLoanController(loanInvestmentService);

        Mockito.when(loanInvestmentService.createInvestment(anyObject())).thenReturn(true);

        ResponseEntity response = investLoanController.investLoan(p);

        assertEquals(ResponseEntity.ok("Investment into loan 2 has been created"), response);
    }

    @Test
    public void loanInvestmentTestRespondsWithError() throws Exception {

        InvestLoanController investLoanController = new InvestLoanController(loanInvestmentService);

        Mockito.when(loanInvestmentService.createInvestment(anyObject())).thenThrow(new IllegalArgumentException("null"));

        ResponseEntity response = investLoanController.investLoan(anyObject());

        assertEquals(ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Bad request: null"), response);
    }

}
